# Axum-Ruma

`axum-ruma` provides [Ruma](https://github.com/ruma/ruma) adapters for [axum](https://github.com/tokio-rs/axum/) web framework.

## Authorization layer

The `MatrixAuthorizationLayer` layer is a `tower` layer which handles the authentication and autorization layer, which can be injected to `axum`'s router.

In order to be able to authenticate the requests, this layer requires two collaborators:

- `ClientAuthorizer`: Provides the request's access token and returns a `ClientIdentity` enum. 
- `FederationKeyProvider`: Provides the required public keys so the federation requests may be authorized.

An `Identity` enum will be stored in the request's extension if the request has been authorized. 

**Note**: Federation authorization requires consuming the request body (in order to check the signature). In those cases, the layer stores a `ReadBody` struct in the request's extensions, but this will provoke that `axum`s extractors which consumes the request body will not work properly. 

## Axum

A generic `Request` extractor is also provided, which will make available the inner Ruma type. If the `Request` requires authorization (either client or federation), the extractor will check that a correct `Identity` was produced by the previous layer.

A `Response` type is also provided, which allows returning `ruma` types to endpoint return types.
