use axum::{body::Body, response::IntoResponse};
use http::Response;
use ruma::api::OutgoingResponse;
use serde_json::json;

pub struct RumaResponse<T: OutgoingResponse>(pub T);

impl<T: OutgoingResponse> From<T> for RumaResponse<T> {
    fn from(response: T) -> Self {
        Self(response)
    }
}

impl<T: OutgoingResponse> IntoResponse for RumaResponse<T> {
    fn into_response(self) -> axum::response::Response {
        let mut builder = Response::builder();

        match self.0.try_into_http_response::<Vec<u8>>() {
            Ok(response) => {
                for (k, v) in response.headers() {
                    builder = builder.header(k, v);
                }

                let status = http::StatusCode::from_u16(response.status().as_u16()).unwrap();
                let body = response.into_body();

                builder.status(status).body(Body::from(body)).unwrap()
            }
            Err(e) => {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "Errored while building a Ruma response",
                );

                let error = json!({
                    "errcode": "M_UNKNOWN",
                    "error": "internal server error",
                });

                let raw_body = serde_json::to_string_pretty(&error).unwrap();

                builder
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::new(raw_body))
                    .unwrap()
            }
        }
    }
}
