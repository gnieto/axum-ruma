pub mod error;
pub mod layer;

mod request;
mod response;

use error::AuthError;
pub use request::RumaRequest;
pub use response::RumaResponse;
use ruma::{
    serde::Base64, OwnedDeviceId, OwnedServerName, OwnedServerSigningKeyId, OwnedUserId,
    ServerName, ServerSigningKeyId, UserId,
};

pub type AppServiceId = String;

#[derive(Clone)]
pub struct FederationAuthData {
    pub(crate) origin: OwnedServerName,
    pub(crate) destination: Option<OwnedServerName>,
    pub(crate) key: OwnedServerSigningKeyId,
    pub(crate) signature: String,
}

impl FederationAuthData {
    pub fn origin(&self) -> &ServerName {
        &self.origin
    }

    pub fn key(&self) -> &ServerSigningKeyId {
        &self.key
    }

    pub fn signature(&self) -> &str {
        &self.signature
    }
}

#[derive(Clone)]
pub enum Identity {
    Client(ClientIdentity),
    Federation(FederationAuthData),
    Unauthenticated,
}

#[derive(Clone)]
pub enum ClientIdentity {
    Device(OwnedUserId, OwnedDeviceId),
    AppService(AppServiceId, Option<OwnedUserId>),
}

#[async_trait::async_trait]
pub trait FederationKeyProvider: Send + Sync {
    fn server_name(&self) -> OwnedServerName;
    async fn public_key(
        &self,
        server: &ServerName,
        id: &ServerSigningKeyId,
    ) -> Result<Option<Base64>, AuthError>;
}

#[async_trait::async_trait]
pub trait ClientAuthorizer: Send + Sync {
    async fn authorize(
        &self,
        token: &str,
        user: Option<&UserId>,
    ) -> Result<ClientIdentity, AuthError>;
}
