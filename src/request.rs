use async_trait::async_trait;
use axum::{
    body::Body,
    extract::{FromRequest, FromRequestParts, OriginalUri, Path},
};
use http::{request::Parts, Uri};
use ruma::api::{
    error::{DeserializationError, FromHttpRequestError},
    IncomingRequest,
};
use serde_json::error::Category;

use crate::{error::RumaError, layer::ReadBody, ClientIdentity, Identity, RumaResponse};

pub struct RumaRequest<T: IncomingRequest> {
    content: T,
    identity: Identity,
}

impl<T: IncomingRequest> RumaRequest<T> {
    pub async fn new<S: Send + Sync>(
        body: Vec<u8>,
        uri: &Uri,
        mut req: Parts,
        state: &S,
        identity: Identity,
    ) -> Result<Self, ruma::api::client::Error> {
        let mut new_request = ::http::Request::builder()
            .method(req.method.clone())
            .uri(uri);
        let path_params: Path<Vec<String>> = Path::<_>::from_request_parts(&mut req, state)
            .await
            .map_err(|e| {
            tracing::error!(err = e.to_string(), "could not find path params");
            RumaError::Deserialization
        })?;
        let mut path_params = path_params.0;
        let any_path = T::METADATA.history.all_paths().next();
        if let Some(path) = any_path {
            let params = path.matches("/:").count();

            for _ in path_params.len()..params {
                path_params.push("".to_string());
            }
        }

        for (k, v) in req.headers.iter() {
            new_request = new_request.header(k.clone(), v.clone());
        }

        let identity = match T::METADATA.authentication {
            ruma::api::AuthScheme::None | ruma::api::AuthScheme::AccessTokenOptional => identity,
            ruma::api::AuthScheme::AccessToken => {
                if let Identity::Client(_) = &identity {
                    identity
                } else {
                    return Err(RumaError::MissingToken.into());
                }
            }
            ruma::api::AuthScheme::ServerSignatures => {
                if let Identity::Federation(_) = &identity {
                    identity
                } else {
                    return Err(RumaError::MissingToken.into());
                }
            }
            ruma::api::AuthScheme::AppserviceToken => match &identity {
                Identity::Client(ClientIdentity::AppService(_, _)) => identity,
                Identity::Client(_) | Identity::Federation(_) => {
                    return Err(RumaError::MissingToken.into());
                }
                Identity::Unauthenticated => Identity::Unauthenticated,
            },
            ruma::api::AuthScheme::AppserviceTokenOptional => match &identity {
                Identity::Client(ClientIdentity::AppService(_, _)) => identity,
                Identity::Federation(_) => {
                    return Err(RumaError::MissingToken.into());
                }
                Identity::Client(_) | Identity::Unauthenticated => Identity::Unauthenticated,
            },
        };

        tracing::debug!(
            body = std::str::from_utf8(&body).unwrap(),
            "incoming request body",
        );

        let http_req = new_request.body(body).map_err(|_| RumaError::MissingBody)?;

        let content =
            T::try_from_http_request::<_, String>(http_req, &path_params).map_err(|e| {
                tracing::error!(
                    err = e.to_string(),
                    "could not deserialize incoming request",
                );

                match e {
                    FromHttpRequestError::Deserialization(DeserializationError::Utf8(_)) => {
                        RumaError::InvalidJson
                    }
                    FromHttpRequestError::Deserialization(DeserializationError::Json(json)) => {
                        match json.classify() {
                            Category::Syntax => RumaError::InvalidJson,
                            _ => RumaError::Deserialization,
                        }
                    }
                    _ => RumaError::Deserialization,
                }
            })?;

        Ok(RumaRequest { content, identity })
    }

    pub fn get(&self) -> &T {
        &self.content
    }

    pub fn take(self) -> (T, Identity) {
        (self.content, self.identity)
    }
}

use http::Request;

#[async_trait]
impl<T, S> FromRequest<S> for RumaRequest<T>
where
    T: IncomingRequest + Send + Sync,
    S: Send + Sync,
{
    type Rejection = RumaResponse<ruma::api::client::Error>;

    async fn from_request(mut req: Request<Body>, state: &S) -> Result<Self, Self::Rejection> {
        let identity = req
            .extensions()
            .get::<Identity>()
            .ok_or_else(|| RumaResponse(RumaError::Unauthorized.into()))?
            .to_owned();
        let maybe_read_body = req.extensions_mut().remove::<ReadBody>();
        let (mut parts, body) = req.into_parts();

        let body = match maybe_read_body {
            Some(body) => body.0,
            None => {
                // TODO: Add limits to body's length
                let body = axum::body::to_bytes(body, usize::MAX)
                    .await
                    .map_err(|_| RumaResponse(RumaError::MissingBody.into()))?;
                body.to_vec()
            }
        };

        let original_uri = OriginalUri::from_request_parts(&mut parts, state)
            .await
            .map_err(|_| RumaResponse(RumaError::MissingBody.into()))?;

        RumaRequest::new(body, &original_uri.0, parts, state, identity)
            .await
            .map_err(RumaResponse)
    }
}
