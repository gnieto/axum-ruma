use std::{
    convert::Infallible,
    task::{Context, Poll},
};

use axum::body::to_bytes;
use axum::{
    body::Body,
    response::{IntoResponse, Response},
};
use axum_extra::headers::{
    authorization::{Authorization, Bearer, Credentials},
    HeaderMapExt,
};
use futures::future::BoxFuture;
use http::{request::Parts, Request};
use ruma::api::federation::authentication::XMatrix;
use ruma::{
    signatures::{verify_json, PublicKeyMap, PublicKeySet},
    CanonicalJsonObject, CanonicalJsonValue, OwnedUserId,
};
use serde::Deserialize;
use tower::{Layer, Service};

use crate::{
    error::AuthError, ClientAuthorizer, FederationAuthData, FederationKeyProvider, Identity,
    RumaResponse,
};

pub enum AuthenticationToken {
    Client(String, Option<OwnedUserId>),
    Federation(FederationAuthData),
    None,
}

#[derive(Deserialize, Default)]
struct QueryParameters {
    access_token: Option<String>,
    user_id: Option<OwnedUserId>,
}

#[derive(Clone)]
pub struct MatrixAuthorizationLayer<C: ClientAuthorizer + Clone, F: FederationKeyProvider + Clone> {
    client: C,
    federation: F,
}

impl<C: ClientAuthorizer + Clone, F: FederationKeyProvider + Clone> MatrixAuthorizationLayer<C, F> {
    pub fn new(client: C, federation: F) -> Self {
        Self { client, federation }
    }
}

impl<S, C: ClientAuthorizer + Clone, F: FederationKeyProvider + Clone> Layer<S>
    for MatrixAuthorizationLayer<C, F>
{
    type Service = MatrixAuthorizationService<S, C, F>;

    fn layer(&self, inner: S) -> Self::Service {
        MatrixAuthorizationService {
            inner,
            client: self.client.clone(),
            federation: self.federation.clone(),
        }
    }
}

#[derive(Clone)]
pub struct MatrixAuthorizationService<S, C: ClientAuthorizer, F: FederationKeyProvider> {
    inner: S,
    client: C,
    federation: F,
}

impl<S, C, F> Service<Request<Body>> for MatrixAuthorizationService<S, C, F>
where
    S: Service<Request<Body>, Response = Response> + Clone + Send + 'static,
    S::Future: Send + 'static,
    C: ClientAuthorizer + Clone + 'static,
    F: FederationKeyProvider + Clone + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    // `BoxFuture` is a type alias for `Pin<Box<dyn Future + Send + 'a>>`
    type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, request: Request<Body>) -> Self::Future {
        let mut inner = self.inner.clone();
        let client = self.client.clone();
        let federation = self.federation.clone();

        Box::pin(async move {
            let authorization_token = match token_from_request(&request) {
                Ok(authorization) => authorization,
                Err(e) => {
                    return Ok(RumaResponse(ruma::api::client::Error::from(e)).into_response());
                }
            };

            let (identity, mut request) = match authorization_token {
                AuthenticationToken::Client(token, user_id) => {
                    let identity_result = client
                        .authorize(token.as_str(), user_id.as_ref().map(|u| u.as_ref()))
                        .await;
                    match identity_result {
                        Ok(id) => (Identity::Client(id), request),
                        Err(e) => {
                            return Ok(
                                RumaResponse(ruma::api::client::Error::from(e)).into_response()
                            )
                        }
                    }
                }
                AuthenticationToken::Federation(identity) => {
                    let (parts, body) = request.into_parts();
                    let bytes = to_bytes(body, usize::MAX).await.unwrap();
                    let bytes = bytes.to_vec();

                    let result = authorize_federation_request(
                        &identity,
                        &parts,
                        bytes.as_slice(),
                        &federation,
                    )
                    .await;

                    let mut request = http::Request::from_parts(parts, Body::empty());

                    match result {
                        Ok(_) => {
                            request.extensions_mut().insert(ReadBody(bytes));

                            (Identity::Federation(identity), request)
                        }
                        Err(e) => {
                            return Ok(
                                RumaResponse(ruma::api::client::Error::from(e)).into_response()
                            );
                        }
                    }
                }
                AuthenticationToken::None => (Identity::Unauthenticated, request),
            };

            request.extensions_mut().insert(identity);

            let response: Response = inner.call(request).await?;
            Ok(response)
        })
    }
}

#[derive(Clone)]
pub struct ReadBody(pub Vec<u8>);

async fn authorize_federation_request(
    authorization_data: &FederationAuthData,
    parts: &Parts,
    body: &[u8],
    key_provider: &dyn FederationKeyProvider,
) -> Result<(), AuthError> {
    let canonical_json =
        convert_request_to_canonical_json(authorization_data, parts, body, key_provider).await?;

    let key_map = build_key_map(key_provider, authorization_data).await?;

    verify_json(&key_map, &canonical_json).map_err(|err| {
        tracing::error!(
            err = err.to_string().as_str(),
            key = authorization_data.key.as_str(),
            server = authorization_data.origin.as_str(),
            "invalid signature on incoming federation request"
        );

        AuthError::InvalidSignature
    })?;

    Ok(())
}

async fn build_key_map(
    key_provider: &dyn FederationKeyProvider,
    authorization_data: &FederationAuthData,
) -> Result<PublicKeyMap, AuthError> {
    let public_key = key_provider
        .public_key(&authorization_data.origin, &authorization_data.key)
        .await?
        .ok_or_else(|| {
            AuthError::MissingKey(
                authorization_data.key.to_owned(),
                authorization_data.origin.to_owned(),
            )
        })?;

    let mut key_map = PublicKeyMap::new();
    let mut key_set = PublicKeySet::new();

    key_set.insert(authorization_data.key.to_string(), public_key);
    key_map.insert(authorization_data.origin.to_string(), key_set);

    Ok(key_map)
}

async fn convert_request_to_canonical_json(
    authorization_data: &FederationAuthData,
    parts: &Parts,
    body: &[u8],
    key_provider: &dyn FederationKeyProvider,
) -> Result<CanonicalJsonObject, AuthError> {
    let mut canonical = CanonicalJsonObject::new();

    canonical.insert(
        "method".to_string(),
        CanonicalJsonValue::String(parts.method.to_string()),
    );
    canonical.insert(
        "uri".to_string(),
        CanonicalJsonValue::String(
            parts
                .uri
                .path_and_query()
                .map(|path| path.as_str().to_string())
                .unwrap_or_default(),
        ),
    );
    canonical.insert(
        "origin".to_string(),
        CanonicalJsonValue::String(authorization_data.origin.to_string()),
    );

    let server_name = authorization_data
        .destination
        .clone()
        .unwrap_or_else(|| key_provider.server_name());
    canonical.insert(
        "destination".to_string(),
        CanonicalJsonValue::String(server_name.to_string()),
    );
    if !body.is_empty() {
        let content_obj: CanonicalJsonValue =
            serde_json::from_slice(body).map_err(|_| AuthError::BadCanonicalJson)?;
        canonical.insert("content".to_string(), content_obj);
    }
    let mut json_key = CanonicalJsonObject::new();

    json_key.insert(
        authorization_data.key.to_string(),
        CanonicalJsonValue::String(authorization_data.signature.to_owned()),
    );
    let mut signature = CanonicalJsonObject::new();
    signature.insert(
        authorization_data.origin.to_string(),
        CanonicalJsonValue::Object(json_key),
    );
    canonical.insert(
        "signatures".to_string(),
        CanonicalJsonValue::Object(signature),
    );

    Ok(canonical)
}

fn parse_query<B>(request: &Request<B>) -> Result<QueryParameters, Infallible> {
    let parameters = request
        .uri()
        .query()
        .map(serde_urlencoded::from_str)
        .transpose()
        .unwrap()
        .unwrap_or_default();

    Ok(parameters)
}

fn token_from_request<B>(request: &Request<B>) -> Result<AuthenticationToken, AuthError> {
    let authorization = request.headers().get(http::header::AUTHORIZATION);
    let query_parameters = parse_query(request).unwrap();
    let token = match authorization {
        Some(authorization) => match request.headers().typed_get::<Authorization<Bearer>>() {
            Some(authorization) => AuthenticationToken::Client(
                authorization.token().to_owned(),
                query_parameters.user_id,
            ),
            None => match XMatrix::decode(authorization) {
                None => AuthenticationToken::None,
                Some(xmatrix) => AuthenticationToken::Federation(FederationAuthData {
                    origin: xmatrix.origin,
                    destination: xmatrix.destination,
                    key: xmatrix.key,
                    signature: xmatrix.sig.to_string(),
                }),
            },
        },
        None => {
            if query_parameters.access_token.is_some() {
                AuthenticationToken::Client(
                    query_parameters.access_token.unwrap(),
                    query_parameters.user_id,
                )
            } else {
                AuthenticationToken::None
            }
        }
    };

    Ok(token)
}

#[cfg(test)]
mod test {
    use axum_extra::headers::authorization::Credentials;
    use http::HeaderValue;
    use ruma::api::federation::authentication::XMatrix;

    #[test]
    fn it_can_parse_federated_header() {
        let header_value = r#"X-Matrix origin="localhost:8800",key="ed25519:01GQZ3FFM819W8TM5R2DWYPF4W",sig="9eUq3aNa2ydwm5i/YGaYk/RCzK9J8MRitwDKdZUQV2DpdyBgml6axao1tou3zv8pGsqzgnz43nfERKyyhlQXBQ""#;
        let header = HeaderValue::from_static(header_value);
        let xmatrix = XMatrix::decode(&header).unwrap();

        assert_eq!("localhost:8800", xmatrix.origin);
    }

    #[test]
    fn it_can_parse_federated_header_unquoted() {
        let header_value = r#"X-Matrix origin=localhost:8800,key=ed25519:01GQZ3FFM819W8TM5R2DWYPF4W,sig=9eUq3aNa2ydwm5i/YGaYk/RCzK9J8MRitwDKdZUQV2DpdyBgml6axao1tou3zv8pGsqzgnz43nfERKyyhlQXBQ"#;
        let header = HeaderValue::from_static(header_value);
        let xmatrix = XMatrix::decode(&header).unwrap();

        assert_eq!("localhost:8800", xmatrix.origin);
    }

    #[test]
    fn auth_data_with_unparseable_field_returns_error() {
        let header_value = r#"X-Matrix origin="localhost:invalid",key="ed25519:01GQZ3FFM819W8TM5R2DWYPF4W",sig="9eUq3aNa2ydwm5i/YGaYk/RCzK9J8MRitwDKdZUQV2DpdyBgml6axao1tou3zv8pGsqzgnz43nfERKyyhlQXBQ""#;
        let header = HeaderValue::from_static(header_value);

        let xmatrix = XMatrix::decode(&header);

        assert!(xmatrix.is_none());
    }

    #[test]
    fn auth_data_with_missing_field_returns_error() {
        let header_value = r#"X-Matrix key="ed25519:01GQZ3FFM819W8TM5R2DWYPF4W",sig="9eUq3aNa2ydwm5i/YGaYk/RCzK9J8MRitwDKdZUQV2DpdyBgml6axao1tou3zv8pGsqzgnz43nfERKyyhlQXBQ""#;
        let header = HeaderValue::from_static(header_value);

        let xmatrix = XMatrix::decode(&header);

        assert!(xmatrix.is_none());
    }
}
