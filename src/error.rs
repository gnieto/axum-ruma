use http::StatusCode;
use ruma::{api::client::error::ErrorBody, OwnedServerName, OwnedServerSigningKeyId};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum AuthError {
    #[error("Bad canonical json")]
    BadCanonicalJson,
    #[error("Missing key {0} and server {1}")]
    MissingKey(OwnedServerSigningKeyId, OwnedServerName),
    #[error("Invalid signature")]
    InvalidSignature,
    #[error("Appsservice user exclusive")]
    AppserviceUserExclusive,
    #[error("Token has expired")]
    ExpiredToken,
    #[error("Token error {0}")]
    TokenError(String),
    #[error("Unparseable challenge field {0}")]
    UnparseableChallengeKey(String),
    #[error("Malformed challenge")]
    MalformedChallenge,
}

impl From<AuthError> for ruma::api::client::Error {
    fn from(e: AuthError) -> Self {
        match e {
            AuthError::ExpiredToken => ruma::api::client::error::Error::new(
                StatusCode::UNAUTHORIZED,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::UnknownToken { soft_logout: true },
                    message: e.to_string(),
                },
            ),
            AuthError::TokenError(e) => ruma::api::client::error::Error::new(
                StatusCode::UNAUTHORIZED,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::UnknownToken { soft_logout: false },
                    message: e.to_string(),
                },
            ),
            AuthError::BadCanonicalJson => ruma::api::client::error::Error::new(
                StatusCode::BAD_REQUEST,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::BadJson,
                    message: e.to_string(),
                },
            ),
            AuthError::MalformedChallenge | AuthError::UnparseableChallengeKey(_) => {
                ruma::api::client::error::Error::new(
                    StatusCode::BAD_REQUEST,
                    ErrorBody::Standard {
                        kind: ruma::api::client::error::ErrorKind::InvalidParam,
                        message: e.to_string(),
                    },
                )
            }
            _ => ruma::api::client::error::Error::new(
                StatusCode::INTERNAL_SERVER_ERROR,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::Unknown,
                    message: e.to_string(),
                },
            ),
        }
    }
}

#[derive(Debug, Error)]
pub enum RumaError {
    #[error("Missing body")]
    MissingBody,
    #[error("Invalid json")]
    InvalidJson,
    #[error("Deserialization error")]
    Deserialization,
    #[error("Unauthorized")]
    Unauthorized,
    #[error("Missing token")]
    MissingToken,
}

impl From<RumaError> for ruma::api::client::Error {
    fn from(e: RumaError) -> Self {
        match e {
            RumaError::InvalidJson => ruma::api::client::error::Error::new(
                StatusCode::BAD_REQUEST,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::NotJson,
                    message: e.to_string(),
                },
            ),
            RumaError::Deserialization => ruma::api::client::error::Error::new(
                StatusCode::BAD_REQUEST,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::BadJson,
                    message: e.to_string(),
                },
            ),
            RumaError::Unauthorized => ruma::api::client::error::Error::new(
                StatusCode::UNAUTHORIZED,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::Unauthorized,
                    message: e.to_string(),
                },
            ),
            RumaError::MissingToken => ruma::api::client::error::Error::new(
                StatusCode::UNAUTHORIZED,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::MissingToken,
                    message: e.to_string(),
                },
            ),
            _ => ruma::api::client::error::Error::new(
                StatusCode::INTERNAL_SERVER_ERROR,
                ErrorBody::Standard {
                    kind: ruma::api::client::error::ErrorKind::Unknown,
                    message: e.to_string(),
                },
            ),
        }
    }
}
